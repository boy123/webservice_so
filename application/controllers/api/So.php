<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;


class So extends REST_Controller {

	public function index_get()
	{
		
	}

	public function login_post()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$cek = $this->db->query("SELECT * FROM app_user WHERE username='$username' and password='$password' ");
		if ($cek->num_rows() > 0) {
			$user = $cek->row();
			$token = base64_encode($username.":".$password);
			$message = [
				'error' => "true",
				'message' => "success login",
				'data' => [
					[
						'id_user' => $user->id_user,
			            'username' => $user->username,
			            'nama_lengkap' => $user->nama_lengkap,
			            'token' => $token
					]
				]
	            
	        ];

	        $this->db->where('id_user', $user->id_user);
	        $this->db->update('app_user', [
	        	'token' => $token,
	        	'last_login' => get_waktu(),
	        	'status_login_api' => 'y'
	        ]);

	        $this->set_response($message, REST_Controller::HTTP_OK);
			
		} else {
		   	$message = [
				'error' => "false",
				'message' => "gagal login",
				'data' => []
	        ];
	        $this->set_response($message, REST_Controller::HTTP_OK);
		}
	}

	public function so_sync_post()
	{
		$json = $this->input->post('so_request');
		$data_request = json_decode($json);

		foreach ($data_request->so_header as $row) {

			$this->db->where('so_no', $row->soNo);
			$checkHeader = $this->db->get('so_header');
			if ($checkHeader->num_rows() > 0) {
				// update

				$data = array(
					"acc_no" => $row->accNo,
					"delivery1" => $row->delivery1,
					"delivery2" => $row->delivery2,
					"delivery3" => $row->delivery3,
					"delivery4" => $row->delivery4,
					"branch" => $row->branch,
					"date" => $row->date,
					"ref_doc" => $row->refDoc,
					"sales_agent" => $row->salesAgent,
					"subtotal" => $row->subTotal,
					"taxable_amount" => $row->taxableAmount,
					"ppn" => $row->ppn,
					"currency_code" => $row->currencyCode,
					"rate" => $row->rate,
					"local_total" => $row->localTotal,
					"total" => $row->total,
					"dibayar" => $row->dibayar,
					"status" => $row->status,
					"created_at" => get_waktu(),
					"created_by" => $row->createdBy,
					"updated_at" => get_waktu(),
					"updated_by" => $row->updatedBy
				);
				$this->db->where('so_no', $row->soNo);
				$this->db->update('so_header', $data);

			} else {

				//insert
				$data = array(
					"so_no" => $row->soNo,
					"acc_no" => $row->accNo,
					"delivery1" => $row->delivery1,
					"delivery2" => $row->delivery2,
					"delivery3" => $row->delivery3,
					"delivery4" => $row->delivery4,
					"branch" => $row->branch,
					"date" => $row->date,
					"ref_doc" => $row->refDoc,
					"sales_agent" => $row->salesAgent,
					"subtotal" => $row->subTotal,
					"taxable_amount" => $row->taxableAmount,
					"ppn" => $row->ppn,
					"currency_code" => $row->currencyCode,
					"rate" => $row->rate,
					"local_total" => $row->localTotal,
					"total" => $row->total,
					"dibayar" => $row->dibayar,
					"status" => $row->status,
					"created_at" => get_waktu(),
					"created_by" => $row->createdBy,
					"updated_at" => get_waktu(),
					"updated_by" => $row->updatedBy
				);
				$this->db->insert('so_header', $data);

			}

			
		}

		foreach ($data_request->so_detail as $row) {

			$this->db->where('so_no', $row->soNo);
			$checkDetail = $this->db->get('so_detail');
			if ($checkDetail->num_rows() > 0) {
				//update
				$data = array(
					"so_no" => $row->soNo,
					"item_code" => $row->itemCode,
					"qty" => $row->qty,
					"uom" => $row->uom,
					"unit_price" => $row->unitPrice,
					"discount" => $row->discount,
					"subtotal" => $row->subtotal,
					"ppn_code" => $row->ppnCode,
					"ppn_rate" => $row->ppnRate,
					"ppn_amount" => $row->ppnAmount,
					"proj_no" => $row->projNo
				);
				$this->db->where('so_no', $row->soNo);
				$this->db->update('so_detail', $data);

			} else {
				//insert
				$data = array(
					"so_no" => $row->soNo,
					"item_code" => $row->itemCode,
					"qty" => $row->qty,
					"uom" => $row->uom,
					"unit_price" => $row->unitPrice,
					"discount" => $row->discount,
					"subtotal" => $row->subtotal,
					"ppn_code" => $row->ppnCode,
					"ppn_rate" => $row->ppnRate,
					"ppn_amount" => $row->ppnAmount,
					"proj_no" => $row->projNo
				);
				$this->db->insert('so_detail', $data);

			}

			
		}
		$message = [
			'error' => "true",
			'message' => "Sukses Simpan Data",
			'data' => []
        ];
        $this->set_response($message, REST_Controller::HTTP_OK);
	}

	public function master_data_get()
	{
		if ($this->check_token()) {
			$message = [
				'error' => FALSE,
				'message' => "success get master data",
				'data' => [
					[
						'branch',
						'currency',
						'debtor',
						'item',
						'item_price',
						'item_uom',
						'tax'
					]
				]
	        ];
	        $this->set_response($message, REST_Controller::HTTP_OK);
		} else {
			$message = [
				'error' => TRUE,
				'message' => "token tidak berlaku",
				'data' => []
	        ];
	        $this->set_response($message, REST_Controller::HTTP_UNAUTHORIZED);
		}
        
	}


	public function download_master_data_get($table)
	{
		if ($this->check_token()) {

			$SalesAgent = $this->input->get('SalesAgent');
			$data = array();
			if ($table == 'item') {
				$sql = "select * from item where itemcode in 
						(select itemcode from stockdtl where location='$SalesAgent'  
						group by itemcode,[location] 
						having sum(qty)>0 
						)";
				$gettable = $this->db->query($sql);
				foreach ($gettable->result() as $rw) {
					array_push($data, $rw);
				}
			} else {
				if ($table == 'debtor') {
					$this->db->where('SalesAgent', $SalesAgent);
				}
				$gettable = $this->db->get($table);
				foreach ($gettable->result() as $rw) {
					array_push($data, $rw);
				}
			}
			

			$message = [
				'error' => "false",
				'message' => "success get master [$table]",
				'data' => [
					"total_data" => $gettable->num_rows(),
					"table" => $table,
					"master_data" => $data
				]
	        ];
	        $this->set_response($message, REST_Controller::HTTP_OK);
		} else {
			$message = [
				'error' => "true",
				'message' => "token tidak berlaku",
				'data' => []
	        ];
	        $this->set_response($message, REST_Controller::HTTP_UNAUTHORIZED);
		}
        
	}

	private function check_token()
	{
		$headers = getallheaders();
		if (array_key_exists('Authorization', $headers)) {
		    $token = trim(str_replace('Bearer','', $headers['Authorization']));
		    $check = $this->db->query("SELECT * FROM app_user WHERE token='$token'  ");
		    if ($check->num_rows() > 0) {
		    	return TRUE;
		    } else {
		    	return FALSE;
		    }

		} else {
			return FALSE;
		}
	}

}

/* End of file So.php */
/* Location: ./application/controllers/api/So.php */
